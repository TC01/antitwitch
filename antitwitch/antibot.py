#!/usr/bin/env python

# A lot of thos code is *really* awful and hasn't been cleaned up.
# The whole logging stuff is pretty bad.

# This was the *old* version of an IRC bot. The newer one has a nicer backend
# for some of this stuff that is in a separate library I didn't want to include
# right now. So I went with this one.

# I will probably improve the code and make it depend on that other library
# someday (probably when I clean up *that* code and release it...)

# I have problems.

# -- TC01 (bjr) (Ben Rosser) (etc)

# Python libraries (preinstalled)
import datetime
import hashlib
import os
import random
import sys
import subprocess
import time
import urllib

# Twisted libraries (dependency- you must install zope.interface as well)
from twisted.words.protocols import irc
from twisted.internet import protocol
from twisted.internet import reactor

# Reverse import antitwitch
import antitwitch

# Some globals- you may need to change!
consolelog = False					# Log activity to console
writelog = False					# Write channel logs
otherchan = []						# Other channels to join
new_nickname = "AntitwitchBot"		# Change nickname when we connect.

class AntiBot(irc.IRCClient):

	def _get_nickname(self):
		return self.factory.nickname
	nickname = property(_get_nickname)

	def signedOn(self):
		global otherchan
		self.join(self.factory.channel)
		if len(otherchan) != 0:
			for chan in otherchan:
				self.join(chan)

	def joined(self, channel):
		self.logPrint(channel, ("%s has joined %s" % (self.nickname, channel)))
		print "[irc] Joined " + channel

	def kickedFrom(self, channel, kicker, message):
		self.logPrint(channel, ("%s was kicked by %s [%s]" % (self.nickname, kicker, message)))
		self.join(channel)
		
	def privmsg(self, user, channel, msg):
		#First, if this is a message from a freenode server, spew it at stdout, but don't parse it- just quit
		if '.freenode.net' in user:
			print "[irc] " + msg
			return
		if 'serv' in user:
			print "[irc] " + msg
			return

		#Get rid of the user's hostmask for logging and all other purposes
		shortuser = user[:user.find('!')]
		if channel != self.nickname:
			self.logPrint(channel, "<"+shortuser+"> "+ msg)
		else:
			channel = shortuser
			
		# I *think* we can now just do this?
		print "[irc] Received: " + msg
		antitwitch.receive_msg(msg)

	def logSay(self, channel, msg):
		"""Both log and say a message from the bot"""
		self.msg(channel, msg)
		self.logPrint(channel, "<" + self.nickname + "> "+ msg)
		
	#Logging routines- monitor activity and self.logPrint(channel, ) them
	def userJoined(self, user, channel):
		self.logPrint(channel, ("%s has joined %s" % (user, channel)))

	def userLeft(self, user, channel):
		self.logPrint(channel, ("%s has left %s" % (user, channel)))
		if user in self.operators:
			self.operators.remove(user)
		
	def userQuit(self, user, quitMessage):
		self.logPrint('#jhusps-gaming', ("%s: Quit [%s]" % (user, quitMessage)))
		if user in self.operators:
			self.operators.remove(user)
		
	def userKicked(self, kickee, channel, kicker, message):
		self.logPrint(channel, ("%s was kicked by %s [%s]" % (kickee, kicker, message)))
		
	def topicUpdated(self, user, channel, newTopic):
		self.logPrint(channel, ("%s changes topic to '%s'" % (user, newTopic)))
		
	def action(self, user, channel, data):
		shortlen = user.find('!')
		shortuser = user[:shortlen]
		if shortlen == -1:
			shortuser = user
		self.logPrint(channel, ("* %s %s" % (shortuser, data)))
		
	def userRenamed(self, oldname, newname):
		self.logPrint('#jhusps-gaming', ("%s is now known as %s" % (oldname, newname)))
		
	def modeChanged(self, user, channel, set, modes, args):
		#Get the + or - string
		strset = '-'
		if set:
			strset = '+'
			
		#Get the shortuser (cut the hostmask)
		shortlen = user.find('!')
		shortuser = user[:user.find('!')]
		if shortlen == -1:
			shortuser = user
		
		#Determine if it's being set on a user or not
		try:
			setuser = args[0]
		except:
			setuser = ''
		
		#Output log information
		self.logPrint(channel, ("%s sets mode %s%s %s" % (shortuser, strset, modes, setuser)))

		#If this is operator mode- o- add or remove operator status to operator array
		if ('o' in modes and setuser != ""):
			if set:
				if not user in self.operators:
					self.operators.append(setuser)
			else:
				try:
					self.operators.remove(setuser)
				except IndexError:
					pass

	def logPrint(self, channel, message):
		"""Function to print activity to the command line and then log it to a file on my system."""
		#Logging globals
		global consolelog
		global writelog
		
		now = time.localtime()
		hour = fixTime(str(now[3]))
		minute = fixTime(str(now[4]))
		second = fixTime(str(now[5]))
		fullmsg = '[' + fixTime(hour) + ':' + fixTime(minute) + ':' + fixTime(second) + '] ' + message
		channame = (channel.upper())[1:]

		#Output the data to the console
		if consolelog:
			print fullmsg

		#Write data to output file
		if writelog:
			pathroot = os.path.join(os.getcwd(), channame, '')
			date = str(datetime.date.today())
			logfile = pathroot + channame + '.' + date 
			logs = open(logfile, 'a')
			logs.write(fullmsg + '\n')
			logs.close()

class AntiBotFactory(protocol.ClientFactory):
	protocol = AntiBot

	#Name temporary...
	def __init__(self, channel, nickname="AntitwitchBot"):
		global new_nickname
		if new_nickname != nickname:
			nickname = new_nickname
		self.channel = channel
		self.nickname = nickname

	def clientConnectionLost(self, connector, reason):
		print "[irc] Lost connection (%s), attempting reconnct." % (reason,)
		connector.connect()

	def clientConnectionFailed(self, connector, reason):
		print "[irc] Could not connect: %s" % (reason,)

def fixTime(string):
	"""Utility to fix problems for a 'time' string."""
	if len(string) == 1:
		string = '0' + string
	return string

def startBot(channel, network, port, nickname):
	global new_nickname
	new_nickname = nickname
	reactor.connectTCP(network, port, AntiBotFactory(channel))
	reactor.run()
	
def main():
	"""Function to handle user input."""	
	network = 'chat.freenode.net'
	print ""
	# So, you can also just run the bot separately.
	reactor.connectTCP(network, 6665, AntiBotFactory('#jhusps-gaming'))
	reactor.run()
	
if __name__ == "__main__":
	main()
