import re
import subprocess
import time

# Import pykeyboard, from PyUserInput
import pykeyboard

# Import our IRC bot
import antibot

# Set up the keyboard.
k = pykeyboard.PyKeyboard()

# The emulator binary.
vbam = "gvbam"

# Stolen from pykeyboard, added so we can delay between press and release.
def tap_key_better(keyboard, character='', n=1, interval=0, length=0.05):
	"""Press and release a given character key n times. Delay between press/release."""
	for i in range(n):
		keyboard.press_key(character)
		time.sleep(length)
		keyboard.release_key(character)
		time.sleep(interval)

def start_irc_bot(channel, network, port, username):
	# This starts the bot, in theory it should never return...
	# Add stuff to kill the bot cleanly.
	antibot.startBot(channel, network, port, username)

def receive_msg(s):
	s.lower()
	cmds = re.split("(\d)", s)
	if len(cmds) == 1:
		run_cmd(cmds[0], 1)
	else:
		for i in range(0, len(cmds) - 1, 2):
			run_cmd(cmds[i], cmds[i+1])

def run_cmd(s, n):
	try:
		if s == "start":
			tap_key_better(k, "\n", int(n), 1)
		elif s == "select":
			tap_key_better(k, k.backspace_key, int(n), 1)
		elif s == "a":
			tap_key_better(k, "z", int(n), 1)
		elif s == "b":
			tap_key_better(k, "b", int(n), 1)
		elif s == "up":
			tap_key_better(k, k.up_key, int(n), 1)
		elif s == "down":
			tap_key_better(k, k.down_key, int(n), 1)
		elif s == "left":
			tap_key_better(k, k.left_key, int(n), 1)
		elif s == "right":
			tap_key_better(k, k.right_key, int(n), 1)
	except:
		print "[antitwitch] ERROR: Something has gone wrong."
		return

def start_vbam(rom):
	# For now, just use subprocess.
	process = subprocess.Popen([vbam, rom])
	print "[vbam] Launched " + rom
