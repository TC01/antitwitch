#!/usr/bin/env python

from distutils.core import setup

setup(	name = "antitwitch",
        version = "0.1",
		description = "Crowd gaming, ala Twitch Plays Pokemon.",
        long_description = "Forwards keystrokes from an IRC channel to an emulator for old video games.",
        author = "Ben Rosser",
        license = "MIT",
        author_email = "rosser.bjr@gmail.com",
        packages = ["antitwitch"],
        scripts = ["antitwitch-start"])
