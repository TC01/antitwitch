antitwitch version 0.1
----------------------

TODO: Write the README.

Dependencies:
-------------

* Python Twisted (python-twisted, Twisted, etc.)
* zope.interface (twisted dependency)
* [PyUserInput](https://github.com/SavinaRoja/PyUserInput)
* Obviously you need vbam / gvbam.

Installing:
-----------

It is now somewhat easier to install!

After installing all the dependencies listed (through various means), you just
need to do:

```
sudo python setup.py install
```

Running:
--------

Obtain a copy of the VBA ROMs you wish to use, through whatever means.
(NOTE: We will NOT distribute these files).

Then:

```
antitwitch-start [ROM FILE]
```

Legal:
------

This program was developed by:

* Ben Rosser <rosser.bjr@gmail.com>
* Kirk Trombley

For use at a LAN party.

This is not endorsed or approved by ANYONE else. The code is released under
the MIT license. See that file for more.
